let title = document.getElementsByClassName('title')[0];
let header = document.getElementsByTagName('header')[0];
let bg = document.querySelector('.bg img');
let landingPage = document.getElementsByClassName('landing-page')[0];
let recentAchievement = document.getElementsByClassName('recent-achievement')[0];
let tagline = document.getElementsByClassName('tagline')[0];
let child = header.children;
let height = document.documentElement.clientHeight;
let share = document.querySelector('.share');
let learnMore = document.querySelector('.title a');

const checkStyle = () => {
    if (CSS.supports('backdrop-filter', 'blur(30px)')) {
        header.style.background = 'rgba(255, 255, 255, 0.3)';
        header.style.backdropFilter = 'blur(20px) saturate(180%)';
    } else {
        header.style.background = '#f3f3f3';
    }
}
const check0 = (elem, elemToCompare) => {
    if (elemToCompare.scrollTop === 0) {
        elem.style.background = 'none';
        elem.style.backdropFilter = 'blur(0px)';
        elem.children[1].src = './assets/share-white.svg';
        elem.style.height = '15%';
    } else {
        elem.children[1].src = './assets/share-black.svg';
        elem.style.height = '12%';
        checkStyle();
    }
}
const copyToClipboard = () => {
    let link = location.href;
    let input = document.createElement('input');
    input.value = link;
    document.body.appendChild(input);
    input.select();
    document.execCommand("copy");
    document.body.removeChild(input);
    alert('Link Copied!');
}

document.body.addEventListener('scroll', e => {
    let body = document.body;
    let headerHeight = height * 0.1;
    let res = height - headerHeight - body.scrollTop;
    let finRes = res / (height - headerHeight);
    check0(header, body);
    if (body.scrollTop === 0) {
        landingPage.style.filter = 'saturate(100%) brightness(100%)';
    } else {
        landingPage.style.filter = `saturate(${finRes * 100}%) brightness(${finRes * 100}%)`;
    }
})


document.addEventListener('DOMContentLoaded', () => {
        let taglineLeft = document.querySelector('.tagline .left span');
        let taglineRight = document.querySelector('.tagline .right p');
        bg.style.transform = 'scale(1)';
        taglineLeft.style.transform = 'translate(-50%, -50%)';
        taglineRight.style.transform = 'translate(-50%, -50%)';
        [title, recentAchievement].forEach(elem => {
            elem.style.animation = 'fadeIn 1s ease-in-out';
        });
        [title, recentAchievement].forEach(elem => {
            elem.style.animation = 'slideUp 1s ease-in-out';
        });
        Array.from(child).forEach(child => {
            child.style.animation = 'fadeIn 1s ease-in-out';
        })
})

document.querySelectorAll('.mission li').forEach(li => {
    li.classList = 'anim';
    li.dataset.anim = 'fadeIn';
})
document.querySelectorAll('.vision h1 >p, .mission h1 >p, .motto h1 >p').forEach(elem => {
    elem.classList = 'anim';
    if (CSS.supports('backdrop-filter: blur(5px)')) elem.dataset.anim = 'fadeIn';
    else elem.dataset.anim = 'slideFromRight';
})
document.querySelectorAll('.history-title').forEach(elem => {
    elem.classList += ' anim';
    elem.dataset.anim = 'slideFromLeft';
})
document.querySelectorAll('.history-top >img').forEach(elem => {
    elem.classList += ' anim';
    elem.dataset.anim = 'slideFromRight';
})
document.querySelectorAll('.history-content').forEach(elem => {
    elem.classList += ' anim';
    elem.dataset.anim = 'fadeIn';
})
document.querySelectorAll('.contact-content').forEach(elem => {
    elem.classList += ' anim';
    elem.dataset.anim = 'fadeIn';
})

document.querySelectorAll('.logo >img, .flag >img').forEach(elem => {
    if (CSS.supports('backdrop-filter: blur(5px)')) elem.dataset.anim = 'fadeIn';
    else elem.dataset.anim = 'slideFromLeft';
})


    const elements = document.querySelectorAll('.anim');
    let observer = new IntersectionObserver(entries => {
        entries.forEach(entry => {
            let dataAnim = entry.target.dataset.anim.split(',');
            let animation = '';
            dataAnim.forEach((anim, index) => {
                if (index === dataAnim.length - 1) animation += `${anim} 0.5s ease-in-out forwards`;
                else animation += `${anim} 0.5s ease-in-out forwards,`;
            })
            if (entry.isIntersecting) {
                entry.target.style.animation = animation;
            } else {
                entry.target.style.animation = 'none';
            }
        })
    })

    elements.forEach(element => {
        observer.observe(element);
    })

window.onload = ()=>{
    if (window.innerWidth <= 600) {
        location.replace('./mobile/index.html');
    }
}
